import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import {
  ClickAwayListener,
  createStyles,
  Grow,
  IconButton,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Theme,
  Tooltip,
  withStyles,
  WithStyles
} from "@material-ui/core";

import NotificationsIcon from '@material-ui/icons/Notifications';
import usePushNotifications from "./usePushNotifications";
import {AuthenticationContext} from "../Users/AuthenticationContext";

const styles = (theme: Theme) =>
  createStyles({});

interface PushNotificationsDropdownProps extends WithStyles<typeof styles> {
}

function PushNotificationsDropdown(props: PushNotificationsDropdownProps) {
  const {classes} = props;
  const context = useContext(AuthenticationContext);

  const [open, setOpen] = useState(false);
  const anchorRef = useRef<HTMLButtonElement>(null);

  const handleToggle = () => {
    setOpen(prevOpen => !prevOpen);
  };

  const handleClose = useCallback((event: React.MouseEvent<EventTarget>) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return;
    }

    setOpen(false);
  }, [anchorRef]);

  function handleListKeyDown(event: React.KeyboardEvent) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current && !open) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const {onClickSubscribeToPushNotification, loading, error, userConsent, userSubscription, onClickAskUserPermission, onClickSendSubscriptionToPushServer} = usePushNotifications(context.api);

  const enableNotifications = useCallback(async () => {
    if (!userConsent) {
      onClickAskUserPermission();
    }
    if (userConsent && !userSubscription) {
      onClickSubscribeToPushNotification()
    }
    if (userSubscription) {
      console.log(userSubscription);
      onClickSendSubscriptionToPushServer();
    }
  }, [userConsent, userSubscription, onClickAskUserPermission, onClickSubscribeToPushNotification, onClickSendSubscriptionToPushServer]);

  return (
    <div>
      <Tooltip title="Powiadomienia • brak powiadomień">
        <IconButton
          color="inherit"
          ref={anchorRef}
          aria-controls={open ? 'menu-list-grow' : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
        >
          <NotificationsIcon />
        </IconButton>
      </Tooltip>

      <Popper open={open} anchorEl={anchorRef.current} role="popover" transition disablePortal>
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{transformOrigin: placement === 'bottom' ? 'left top' : 'left bottom', left: '-20px'}}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <>
                  {!userSubscription ?
                    <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                      <MenuItem onClick={enableNotifications}>Włącz powiadomienia</MenuItem>
                    </MenuList>
                    : null
                  }
                  <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                    <MenuItem>Powiadomienie</MenuItem>
                    <MenuItem>Powiadomienie 2</MenuItem>
                    <MenuItem>Powiadomienie 3 lorem</MenuItem>
                  </MenuList>
                </>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );

}

export default withStyles(styles)(PushNotificationsDropdown);
