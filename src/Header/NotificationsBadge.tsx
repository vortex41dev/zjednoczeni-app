import React, {useCallback, useState} from 'react';
import {createStyles, IconButton, Theme, Tooltip, withStyles, WithStyles} from "@material-ui/core";
import NotificationsIcon from '@material-ui/icons/Notifications';

const styles = (theme: Theme) =>
  createStyles({});

interface NotificationsBadgeProps extends WithStyles<typeof styles> {
}

function checkNotificationPromise() {
  try {
    Notification.requestPermission().then();
  } catch (e) {
    return false;
  }
  return true;
}

function askNotificationPermission(): Promise<string> {
  return new Promise((resolve, reject) => {
    // function to actually ask the permissions
    function handlePermission(permission: any) {
      // Whatever the user answers, we make sure Chrome stores the information
      if (!('permission' in Notification)) {
        // @ts-ignore
        Notification.permission = permission;
      }

      // set the button to shown or hidden, depending on what the user answers
      if (Notification.permission === 'denied' || Notification.permission === 'default') {
        reject();
      } else {
        resolve(Notification.permission);
      }
    }

    // Let's check if the browser supports notifications
    if (!('Notification' in window)) {
      console.log("This browser does not support notifications.");
      reject("Przeglądarka nie wspiera powiadomień.");
    } else {
      if (checkNotificationPromise()) {
        Notification.requestPermission()
          .then((permission) => {
            handlePermission(permission);
          })
      } else {
        Notification.requestPermission(function (permission) {
          handlePermission(permission);
        });
      }
    }
  });
}

function NotificationsBadge(props: NotificationsBadgeProps) {
  const {classes} = props;

  const [notificationsEnabled, setNotificationsEnabled] = useState(Notification && Notification.permission === 'granted');

  const handleClick = useCallback(() => {
    if (!notificationsEnabled) {
      askNotificationPermission().then(r => {
        console.log("Permission granted!");
        setNotificationsEnabled(true);
      });
    } else {
      const n = new Notification("Hi! ", {tag: 'soManyNotification'});
    }
  }, [notificationsEnabled]);


  return (
    <Tooltip title="Powiadomienia • Brak nowych">
      <IconButton color="inherit" onClick={handleClick}>
        <NotificationsIcon />
      </IconButton>
    </Tooltip>
  );
}

export default withStyles(styles)(NotificationsBadge);
