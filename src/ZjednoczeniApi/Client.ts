import {Prayer} from "../Prayer/PrayersList";
import {Article} from "../Articles/Article";

export interface UserRegistrationData {
  username: string;
  email: string;
  plainPassword: string;
}

export interface OAuthPasswordCredentials {
  username: string;
  password: string;
  client_id: string;
  client_secret: string;
  grant_type: string;
}

export interface UserData {
  id: number;
  username: string;
  email: string;
  enabled: boolean;
  roles: string[];
  meeting_notifications_enabled: boolean;
  takes_part_in_prayer: boolean;
}

export class UserDataC implements UserData {
  email: string = '';
  enabled: boolean = true;
  id: number = 0;
  meeting_notifications_enabled: boolean = false;
  roles: string[] = [];
  takes_part_in_prayer: boolean = false;
  username: string = '';
}

export interface OAuthTokenInfo {
  access_token: string;
  expires_in: number;
  token_type: string;
  scope: any;
  refresh_token: string;
}

export class ApiClient {
  constructor(private basePath: string, private client_id: string, private client_secret: string, private token?: string) {
  }

  public async register(userData: UserRegistrationData): Promise<UserData> {
    const response = await fetch(`${this.basePath}/user`, {
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      body: JSON.stringify(userData) // body data type must match "Content-Type" header
    });
    if (response.status === 400) {
      throw await response.json();
    }
    if (response.status === 201) {
      return await response.json();
    }
    // eslint-disable-next-line no-throw-literal
    throw {error: "Unknown error occured", response: response};
  }

  async updateUser(user: UserData): Promise<UserData> {
    return this.doPut<UserData>(`api/user/${user.id}`, user);
  }

  public async login(username: string, password: string): Promise<OAuthTokenInfo> {
    const credentials: OAuthPasswordCredentials = {
      username: username,
      password: password,
      client_id: this.client_id,
      client_secret: this.client_secret,
      grant_type: 'password',
    };
    const response = await fetch(`${this.basePath}/oauth/v2/token`, {
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      body: JSON.stringify(credentials),
    });
    if (response.status >= 400 && response.status < 500) {
      throw await response.json();
    }
    if (response.status >= 200 && response.status < 300) {
      return await response.json();
    }
    // eslint-disable-next-line no-throw-literal
    throw {error: "Unknown error occured", response: response};
  }

  public async loginWithRefreshToken(refreshToken: string): Promise<OAuthTokenInfo> {
    const credentials = {
      refresh_token: refreshToken,
      client_id: this.client_id,
      client_secret: this.client_secret,
      grant_type: 'refresh_token',
    };
    const response = await fetch(`${this.basePath}/oauth/v2/token`, {
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      body: JSON.stringify(credentials),
    });
    if (response.status >= 400 && response.status < 500) {
      throw await response.json();
    }
    if (response.status >= 200 && response.status < 300) {
      return await response.json();
    }
    // eslint-disable-next-line no-throw-literal
    throw {error: "Unknown error occured", response: response};
  }

  public async getCurrentUser(accessToken: string): Promise<UserData> {
    const response = await fetch(`${this.basePath}/api/current-user`, {
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        'Authorization': `Bearer ${accessToken}`,
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
    });

    if (response.status >= 400 && response.status < 500) {
      throw await response.json();
    }
    if (response.status >= 200 && response.status < 300) {
      return await response.json();
    }
    // eslint-disable-next-line no-throw-literal
    throw {error: "Unknown error occured", response: response};
  }

  static create() {
    let token = null;
    if (sessionStorage.access_token) {
      const {access_token, expires} = JSON.parse(sessionStorage.access_token);
      if (new Date(expires) > new Date()) {
        token = access_token;
      }
    }
    // return new ApiClient('https://zjednoczeni-api.vortex41.pl', '3_2tzw7mw57gkkssssk84gk8wwwcgc4ckcg8gsck4gwss88kks0w', '101og7ob7ry8wkckg4wo8ks4gockggg4g088sggokgo04c0w0w', token);
    return new ApiClient('http://localhost:20081', '1_2lcf8hjctog0gg4socgcws0wsc4c40kg8wsowkg4g4sc8ocgko', '2m02t2wegig4cow8wck4k40kc04s840ccw4s8owwogwo4kgwkg', token);
  }

  async getArticlesList(): Promise<Article[]> {
    return this.doGet('api/article');
  }

  async getArticle(id: number): Promise<Article> {
    return this.doGet(`api/article/${id}`);
  }

  async updateArticle(article: Article): Promise<Article> {
    if (article.id) {
      return this.doPut<Article>(`api/article/${article.id}`, article);
    }
    return this.doPost<Article>(`api/article`, article);
  }

  public async deleteArticle(id: number): Promise<any> {
    return this.doDelete<Article>(`api/article/${id}`);
  }

  async getPrayerList(): Promise<Prayer[]> {
    return this.doGet('api/prayer');
  }

  async getPrayer(id: number): Promise<Prayer> {
    return this.doGet(`api/prayer/${id}`);
  }

  async createPrayer(prayer: Prayer): Promise<Prayer> {
    return this.doPost<Prayer>(`api/prayer`, prayer);
  }

  async updatePrayer(prayer: Prayer): Promise<Prayer> {
    return this.doPut<Prayer>(`api/prayer/${prayer.id}`, prayer);
  }

  public async deletePrayer(id: number): Promise<any> {
    return this.doDelete<Prayer>(`api/prayer/${id}`);
  }

  async getUsers(): Promise<UserData[]> {
    return this.doGet('api/users');
  }

  public async validateUsernameEmail(userId: number, dataToValidate: { email?: string, username?: string }) {
    return this.doPost<any, any>(`api/user/${userId}/validation`, dataToValidate);
  }

  public async getUser(userId: number): Promise<UserData> {
    return this.doGet<UserData>(`api/user/${userId}`);
  }

  public async doGet<T>(path: string): Promise<T> {
    return this.doFetch('GET', path);
  }

  public async doDelete<T>(path: string): Promise<any> {
    return this.doFetch('DELETE', path);
  }

  public async doPut<T, U = T>(path: string, content: T): Promise<U> {
    return this.doFetch('PUT', path, {body: JSON.stringify(content)});
  }

  public async doPost<T, U = T>(path: string, content: T): Promise<U> {
    return this.doFetch('POST', path, {body: JSON.stringify(content)});
  }

  public async doFetch(method: string, path: string, init?: RequestInit): Promise<any> {
    if (!init) {
      init = {};
    }
    const response = await fetch(`${this.basePath}/${path}`, {...this.getBaseInit(method), ...init});
    if (response.status >= 400 && response.status < 500) {
      throw await response.json();
    }
    if (response.status >= 200 && response.status < 300) {
      if (response.status !== 204 && response.body && response.body.toString()) {
        return await response.json();
      }
      return null;
    }
    // eslint-disable-next-line no-throw-literal
    throw {error: "Unknown error occurred", response: response};
  }

  private getBaseInit(method: string): RequestInit {
    return {
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: this.buildHeaders(method),
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      credentials: 'include',
    };
  }

  private buildHeaders(method: string) {
    const headers: any = {};
    if (this.token) {
      headers.Authorization = `Bearer ${this.token}`;
      // headers['Cookie'] = `XDEBUG_SESSION=PHPSTORM`;
    }

    if (method === 'PUT' || method === 'POST') {
      headers['Content-Type'] = 'application/json';
    }
    return headers;
  }
}
