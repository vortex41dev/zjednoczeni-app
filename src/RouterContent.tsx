import React, {useContext} from 'react';
import {Route, Switch} from "react-router-dom";


import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import Registration from "./Users/Registration/Registration";
import Login from "./Users/Login/Login";
import PrayerList from "./Prayer/PrayersList";
import {AuthenticationContext} from "./Users/AuthenticationContext";
import PrayerEditPage from "./Prayer/PrayerEditPage";
import UsersListPage from "./Users/UsersList/UsersListPage";
import CurrentUserNotificationSettingsPage from "./Users/UserNotificationSettings/CurrentUserNotificationSettingsPage";
import UserNotificationSettingsPage from "./Users/UserNotificationSettings/UserNotificationSettingsPage";
import AddPrayerPage from "./Prayer/AddPrayerPage";
import ArticlesFeedPage from "./Articles/ArticlesFeedPage";
import ArticleEditPage from "./Articles/ArticleEdit/ArticleEditPage";
import ArticleNewPage from "./Articles/ArticleEdit/ArticleNewPage";

const styles = (theme: Theme) =>
  createStyles({});

export interface RouterContentProps extends WithStyles<typeof styles> {
}

function RouterContent(props: RouterContentProps) {
  const context = useContext(AuthenticationContext);

  return (
    <Switch>
      <Route path="/rejestracja">
        <Registration apiClient={context.api} />
      </Route>
      <Route path="/zaloguj">
        <Login apiClient={context.api} />
      </Route>
      <Route path="/admin/aktualnosci/nowa" component={ArticleNewPage} />
      <Route path="/admin/aktualnosci/:id/edytuj" component={ArticleEditPage} />
      <Route path="/admin/modlitwy/nowa" component={AddPrayerPage} />
      <Route path="/admin/modlitwy/:id" component={PrayerEditPage} />
      <Route path="/admin/modlitwy">
        <PrayerList apiClient={context.api} />
      </Route>
      <Route path="/admin/uzytkownicy/:id" component={UserNotificationSettingsPage} />
      <Route path="/admin/uzytkownicy">
        <UsersListPage apiClient={context.api} />
      </Route>
      <Route path="/uzytkownik/ustawienia">
        <CurrentUserNotificationSettingsPage />
      </Route>
      <Route path="/" component={ArticlesFeedPage} />
    </Switch>
  );
}

export default withStyles(styles)(RouterContent);
