import React, {useCallback} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";


const styles = (theme: Theme) =>
  createStyles({});

interface ConfirmDialogProps extends WithStyles<typeof styles> {
  isOpened: boolean;
  title: string;
  name: string;
  text: string;
  cancelText?: string;
  confirmText?: string;
  handleClose: () => void;
  onConfirmed: () => void;
  onCancelled?: () => void;
}

function ConfirmDialog(props: ConfirmDialogProps) {
  const {classes} = props;

  const {isOpened, handleClose, onCancelled, onConfirmed} = props;

  const cancelText = props.cancelText || 'Anuluj';
  const confirmText = props.confirmText || 'OK';

  const baseId = `alert-dialog-${props.name}`;
  const titleId = `${baseId}-title`;
  const descriptionId = `${baseId}-description`;

  const cancelCallback = useCallback(() => {
    handleClose();
    onCancelled && onCancelled();
  }, [handleClose, onCancelled]);

  const confirmCallback = useCallback(() => {
    handleClose();
    onConfirmed();
  }, [handleClose, onConfirmed]);


  return (
    <Dialog
      open={isOpened}
      onClose={handleClose}
      aria-labelledby={titleId}
      aria-describedby={descriptionId}
    >
      <DialogTitle id={titleId}>
        {props.title}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id={descriptionId}>
          {props.text}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={cancelCallback} color="primary">
          {cancelText}
        </Button>
        <Button onClick={confirmCallback} color="primary" autoFocus>
          {confirmText}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(styles)(ConfirmDialog);
