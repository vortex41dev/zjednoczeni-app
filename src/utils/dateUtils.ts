function padWith0(val: number, len = 2) {
  const s = val.toString();
  if (s.length === len) {
    return s;
  }
  return '0'.repeat(len - s.length) + s;
}

export function formatDateAtom(d: Date): string {
  const ymd = `${d.getFullYear()}-${padWith0(d.getMonth()+1)}-${padWith0(d.getDate())}`;
  const time = `${padWith0(d.getHours())}:${padWith0(d.getMinutes())}:${padWith0(d.getSeconds())}`;
  const tzOffsetMinutes: number = d.getTimezoneOffset();
  const tzOffsetHr = Math.floor(tzOffsetMinutes / 60);
  const tzOffsetMin = tzOffsetMinutes % 60;
  return `${ymd}T${time}${tzOffsetHr >= 0 ? '-' : '+'}${padWith0(Math.floor(Math.abs(tzOffsetHr)))}:${padWith0(tzOffsetMin)}`;
}
