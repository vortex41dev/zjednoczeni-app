import React, {useCallback, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {ApiClient} from "../ZjednoczeniApi/Client";
import {Prayer} from "./PrayersList";
import ConfirmDialog from "../Dialog/ConfirmDialog";


const styles = (theme: Theme) =>
  createStyles({});

interface PrayerDeleteButtonProps extends WithStyles<typeof styles> {
  prayer: Prayer;
  apiClient: ApiClient;
  onDeleted?: () => void;
}

function PrayerDeleteButton(props: PrayerDeleteButtonProps) {
  const {prayer, apiClient, onDeleted} = props;

  const [isOpened, setOpened] = useState<boolean>(false);

  const doDeletePrayer = useCallback(() => {
    apiClient.deletePrayer(prayer.id)
      .then(() => {
        onDeleted && onDeleted();
      });

  }, [onDeleted, prayer, apiClient]);

  return (
    <>
      <Button type="button" variant="contained" onClick={() => setOpened(true)} color="secondary">Usuń</Button>
      <ConfirmDialog
        name={`prayer-delete-confirm-${prayer.id}`}
        isOpened={isOpened}
        title={`Usuwanie modlitwy ${prayer.name}`}
        text={`Na pewno chcesz usunąć modlitwę "${prayer.name}?`}
        handleClose={() => setOpened(false)}
        onConfirmed={() => doDeletePrayer()}
      />
    </>
  );
}

export default withStyles(styles)(PrayerDeleteButton);
