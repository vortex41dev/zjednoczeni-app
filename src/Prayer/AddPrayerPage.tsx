import React, {useContext, useEffect, useState} from 'react';
import {createStyles, Grid, Theme, withStyles, WithStyles} from "@material-ui/core";
import {RouteComponentProps} from 'react-router-dom';
import {AuthenticationContext} from "../Users/AuthenticationContext";
import PrayerEdit from "./PrayerEdit";
import LoadingIndicator from "../Layout/LoadingIndicator";
import {Prayer} from "./PrayersList";
import CardWithTitle from "../Layout/CardWithTitle";


const styles = (theme: Theme) =>
  createStyles({});

interface AddPrayerPageProps extends WithStyles<typeof styles>, RouteComponentProps<{ id: string }> {

}

function AddPrayerPage(props: AddPrayerPageProps) {
  const {classes} = props;
  const context = useContext(AuthenticationContext);

  const [prayer, setPrayer] = useState<Prayer>({
    id: 0,
    name: '',
    date_since: null,
    date_to: null,
    announcement_notification_content: '',
    remind_notification_content: '',
    announcement_notification_time: null,
    remind_notification_time: null,
  });

  return (
    <CardWithTitle title="Nowa modlitwa">
      <PrayerEdit apiClient={context.api} prayer={prayer} />
    </CardWithTitle>
  );
}

export default withStyles(styles)(AddPrayerPage);
