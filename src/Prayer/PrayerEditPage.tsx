import React, {useCallback, useContext, useEffect, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {RouteComponentProps} from 'react-router-dom';
import {AuthenticationContext} from "../Users/AuthenticationContext";
import PrayerEdit from "./PrayerEdit";
import LoadingIndicator from "../Layout/LoadingIndicator";
import {Prayer} from "./PrayersList";
import PrayerDeleteButton from "./PrayerDeleteButton";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";


const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      overflowX: 'auto',
      maxWidth: 936,
      alignSelf: 'center',
    },
    root_fw: {
      width: '100%',
      overflowX: 'auto',
      alignSelf: 'center',
    },
    headerTitle: {
      fontSize: 24,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    headerRow: {
      marginBottom: 15,
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  });

interface PrayerEditPageProps extends WithStyles<typeof styles>, RouteComponentProps<{ id: string }> {

}

function PrayerEditPage(props: PrayerEditPageProps) {
  const {classes} = props;
  const context = useContext(AuthenticationContext);
  const id = parseInt(props.match.params.id);

  const [loading, setLoading] = useState<boolean>(true);
  const [prayer, setPrayer] = useState<Prayer | null>(null);

  useEffect(() => {
    setLoading(true);
    context.api.getPrayer(id)
      .then((p: Prayer) => {
        setPrayer(p);
        setLoading(false);
      });
  }, [context.api, id]);

  const onDeleted = useCallback(() => {
    props.history.push('/admin/modlitwy');
  }, [props.history]);

  if (!prayer || loading) {
    return <LoadingIndicator />;
  }

  return (
    <div className={classes.container}>
      <Grid container alignItems="center" className={classes.headerRow}>
        <Grid item xs>
          <Typography variant="h1" color="textSecondary" align="center" className={classes.headerTitle}>
            {prayer.name}
          </Typography>
        </Grid>
        <Grid item>
          <PrayerDeleteButton apiClient={context.api} prayer={prayer} onDeleted={onDeleted} />
        </Grid>
      </Grid>
      <Paper className={classes.root}>
        <PrayerEdit apiClient={context.api} prayer={prayer} />
      </Paper>
    </div>
  );
}

export default withStyles(styles)(PrayerEditPage);
