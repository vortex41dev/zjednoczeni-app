import React, {useContext, useEffect, useState} from 'react';
import {AuthenticationContext} from "../Users/AuthenticationContext";
import {
  createStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  Typography,
  withStyles,
  WithStyles
} from "@material-ui/core";

import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
import {ApiClient} from "../ZjednoczeniApi/Client";
import CardWithTitle from "../Layout/CardWithTitle";
import {Link} from "react-router-dom";
import LoadingIndicator from "../Layout/LoadingIndicator";
import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import TextField from "@material-ui/core/TextField";
import SearchIcon from '@material-ui/icons/Search';
import Button from "@material-ui/core/Button";

export interface Prayer {
  id: number;
  name: string;
  date_since: string | null;
  date_to: string | null;
  announcement_notification_content: string;
  remind_notification_content: string;
  announcement_notification_time: string | null;
  remind_notification_time: string | null;
}


const styles = (theme: Theme) =>
  createStyles({
    paper: {
      maxWidth: 936,
      margin: 'auto',
      overflow: 'hidden',
    },
    headerTitle: {
      fontSize: 24,
      marginBottom: 15,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    messageIcon: {
      marginRight: theme.spacing(2),
      color: '#ff0000'
    },
    root: {
      width: '100%',
      overflowX: 'auto',
    },
    table: {
      minWidth: 600,
    },
    link: {
      textDecoration: 'none',
      color: 'inherit',
    },
    searchBar: {
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    },
    searchInput: {
      fontSize: theme.typography.fontSize,
    },
    block: {
      display: 'block',
    },
    addUser: {
      marginRight: theme.spacing(1),
    },
    contentWrapper: {
      margin: '10px 16px 20px',
    },
  });

export interface PrayerListProps extends WithStyles<typeof styles> {
  apiClient: ApiClient;
}

function PrayerList(props: PrayerListProps) {
  const {classes} = props;
  const authContext = useContext(AuthenticationContext);

  const [prayerList, setPrayerList] = useState<Prayer[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [filterPhrase, setFilterPhrase] = useState<string>('');
  const filterPhraseLc = filterPhrase.toLocaleLowerCase();

  const reloadPrayers = () => {
    setLoading(true);
    props.apiClient.getPrayerList()
      .then((prayers: Prayer[]) => {
        setPrayerList(prayers);
        setLoading(false);
      });
  };

  useEffect(reloadPrayers, [props.apiClient]);

  if (loading) {
    return (
      <CardWithTitle title="Modlitwy">
        <LoadingIndicator />
      </CardWithTitle>
    );
  }

  if (!authContext.user || authContext.user.roles.indexOf('ROLE_ADMIN')) {
    return (
      <Paper>
        <Typography variant="h1" color="textSecondary" align="center"
                    className={classes.headerTitle}>
          <AccessAlarmIcon className={classes.messageIcon} />
          Brak dostępu
        </Typography>
      </Paper>
    );
  }

  return (
    <CardWithTitle title="Modlitwy">
      <AppBar className={classes.searchBar} position="static" color="default" elevation={0}>
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon className={classes.block} color="inherit" />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Szukaj po nazwie"
                InputProps={{
                  disableUnderline: true,
                  className: classes.searchInput,
                  value: filterPhraseLc,
                  onChange: (e) => setFilterPhrase(e.target.value),
                }}
              />
            </Grid>
            <Grid item>
              <Link to="modlitwy/nowa" className={classes.link}><Button type="button" variant="contained"
                                                                        color="primary">Dodaj modlitwę</Button></Link>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <div className={classes.contentWrapper}>
        <Table className={classes.table} aria-label="Modlitwy">
          <TableHead>
            <TableRow>
              <TableCell>Nazwa</TableCell>
              <TableCell align="right">Aktywna od</TableCell>
              <TableCell align="right">Aktywna do</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {prayerList.filter(p => (!filterPhraseLc || p.name.toLocaleLowerCase().indexOf(filterPhraseLc) !== -1)).map((prayer: Prayer) => {
              const sinceDate = prayer.date_since ? new Date(prayer.date_since) : null;
              const toDate = prayer.date_to ? new Date(prayer.date_to) : null;
              return (
                <TableRow key={prayer.id}>
                  <TableCell component="th" scope="row">
                    <Link to={`modlitwy/${prayer.id}`} className={classes.link}>
                      {prayer.name.toString()}
                    </Link>
                  </TableCell>
                  <TableCell
                    align="right">{sinceDate ? sinceDate.toLocaleDateString() : null} {sinceDate ? sinceDate.toLocaleTimeString() : null}</TableCell>
                  <TableCell
                    align="right">{toDate ? toDate.toLocaleDateString() : null} {toDate ? toDate.toLocaleTimeString() : null}</TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </div>
    </CardWithTitle>
  );
}

export default withStyles(styles)(PrayerList);
