import 'date-fns';
import React, {useEffect} from 'react';
import {Button, createStyles, Grid, TextField, Theme, withStyles, WithStyles} from "@material-ui/core";
import {Prayer} from "./PrayersList";
import useForm from "react-hook-form";
import {ApiClient} from "../ZjednoczeniApi/Client";
import {KeyboardDatePicker, KeyboardTimePicker,} from '@material-ui/pickers';
import {formatDateAtom} from "../utils/dateUtils";


const styles = (theme: Theme) =>
  createStyles({
    form: {
      margin: '40px 16px',
      alignSelf: 'center',
    },
    form_row: {
      margin: '10px 8px',
      display: 'flex',
      justifyContent: 'flex-start',
    }
  });

interface PrayerEditProps extends WithStyles<typeof styles> {
  prayer: Prayer;
  apiClient: ApiClient;
}

function PrayerEdit(props: PrayerEditProps) {

  const {classes} = props;

  const {register, setValue, triggerValidation, getValues, handleSubmit, errors} = useForm();

  const values = getValues();

  const onSubmit = (values: any) => {
    const prayerT: Prayer = {...props.prayer};
    prayerT.name = values.name;
    prayerT.date_since = values.date_since ? formatDateAtom(values.date_since) : null;
    prayerT.date_to = values.date_to ? formatDateAtom(values.date_to) : null;
    prayerT.announcement_notification_content = values.announcement_notification_content;
    prayerT.remind_notification_content = values.remind_notification_content;
    prayerT.announcement_notification_time = values.announcement_notification_time ? formatDateAtom(values.announcement_notification_time) : null;
    prayerT.remind_notification_time = values.remind_notification_time ? formatDateAtom(values.remind_notification_time) : null;

    if (prayerT.id) {
      return props.apiClient
        .updatePrayer(prayerT)
        .then(p => Promise.resolve());
    } else {
      return props.apiClient
        .createPrayer(prayerT)
        .then(p => Promise.resolve());
    }
  };

  const errorMsgs: any = {
    name: {
      required: 'Wpisz nazwę',
    },
    announcement_notification_content: {
      required: 'Wpisz treść powiadomienia',
    },
    announcement_notification_time: {
      required: 'Wybierz czas powiadomienia podstawowego'
    },
    remind_notification_content: {
      required: 'Wpisz treść powiadomienia przypominającego',
    },
    remind_notification_time: {
      required: 'Wybierz czas powiadomienia przypominającego'
    },
  };

  const errorsFor = (fieldName: string) => {
    const fieldErrors = errors[fieldName];
    if (!fieldErrors) {
      return null;
    }
    if (fieldErrors.message) {
      return fieldErrors.message;
    }
    return errorMsgs[fieldName][fieldErrors.type];
  };

  useEffect(() => {
    register({name: 'date_since'}, {required: false});
    register({name: 'date_to'}, {required: false});
    register({name: 'announcement_notification_time'}, {required: true});
    register({name: 'remind_notification_time'}, {required: true});
  }, []);

  useEffect(() => {
    if (props.prayer.date_since) {
      setValue('date_since', new Date(props.prayer.date_since));
    }
    if (props.prayer.date_to) {
      setValue('date_to', new Date(props.prayer.date_to));
    }
    if (props.prayer.announcement_notification_time) {
      setValue('announcement_notification_time', new Date(props.prayer.announcement_notification_time));
    }
    if (props.prayer.remind_notification_time) {
      setValue('remind_notification_time', new Date(props.prayer.remind_notification_time));
    }
  }, [props.prayer.announcement_notification_time, props.prayer.date_since, props.prayer.date_to, props.prayer.remind_notification_time]);

  return (
    <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
      <Grid container justify="space-around" direction="column">
        <div className={classes.form_row}>
          <TextField variant="outlined"
                     name="name"
                     fullWidth
                     inputRef={register({required: true})}
                     defaultValue={props.prayer.name}
                     error={!!errors.name}
                     helperText={errorsFor('name')}
                     label="Nazwa"
          />
        </div>
        <div className={classes.form_row}>
          <KeyboardDatePicker
            margin="normal"
            format="yyyy-MM-dd"
            KeyboardButtonProps={{
              'aria-label': 'Ustaw datę od',
            }}
            name="date_since"
            label="Od daty"
            inputVariant="outlined"
            error={!!errors.date_since}
            helperText={errorsFor('date_since')}
            value={values.date_since || props.prayer.date_since}
            onChange={(val: Date | null) => {
              setValue('date_since', val);
              triggerValidation({name: 'date_since'})
            }}
          />
          <KeyboardDatePicker
            margin="normal"
            format="yyyy-MM-dd"
            KeyboardButtonProps={{
              'aria-label': 'Ustaw datę do',
            }}
            name="date_to"
            label="Do daty"
            inputVariant="outlined"
            error={!!errors.date_to}
            helperText={errorsFor('date_to')}
            value={values.date_to || props.prayer.date_to}
            onChange={(val: Date | null) => {
              setValue('date_to', val);
              triggerValidation({name: 'date_to'})
            }}
          />
        </div>
        <div className={classes.form_row}>
          <KeyboardTimePicker
            format="HH:mm"
            name="announcement_notification_time"
            margin="normal"
            KeyboardButtonProps={{
              'aria-label': 'Ustaw czas pierwszego powiadomienia',
            }}
            label="Czas pierwszego powiadomienia"
            inputVariant="outlined"
            error={!!errors.announcement_notification_time}
            helperText={errorsFor('announcement_notification_time')}
            value={values.announcement_notification_time || (props.prayer.announcement_notification_time ? new Date(props.prayer.announcement_notification_time) : null)}
            onChange={(val: Date | null) => {
              setValue('announcement_notification_time', val);
              triggerValidation({name: 'announcement_notification_time'})
            }}
          />
          <KeyboardTimePicker
            format="HH:mm"
            name="remind_notification_time"
            margin="normal"
            label="Czas drugiego przypomnienia"
            KeyboardButtonProps={{
              'aria-label': 'Ustaw czas drugiego powiadomienia przypominającego',
            }}
            inputVariant="outlined"
            error={!!errors.remind_notification_time}
            helperText={errorsFor('remind_notification_time')}
            value={values.remind_notification_time || (props.prayer.remind_notification_time ? new Date(props.prayer.remind_notification_time) : null)}
            onChange={(val: Date | null) => {
              setValue('remind_notification_time', val);
              triggerValidation({name: 'remind_notification_time'})
            }}
          />
        </div>
        <div className={classes.form_row}>
          <TextField name="announcement_notification_content"
                     label="Treść pierwszego powiadomienia"
                     variant="outlined"
                     fullWidth
                     multiline
                     inputRef={register({required: true})}
                     defaultValue={props.prayer.announcement_notification_content}
                     error={!!errors.announcement_notification_content}
                     helperText={errorsFor('announcement_notification_content')}
          />
        </div>
        <div className={classes.form_row}>
          <TextField
            name="remind_notification_content"
            label="Treść powiadomienia przypominającego"
            variant="outlined"
            fullWidth
            multiline
            inputRef={register({required: true})}
            defaultValue={props.prayer.remind_notification_content}
            error={!!errors.remind_notification_content}
            helperText={errorsFor('remind_notification_content')}
          />
        </div>
        <div className={classes.form_row}>
          <Button type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  size="large">Zapisz</Button>
        </div>
      </Grid>
    </form>
  );

}

export default withStyles(styles)(PrayerEdit);
