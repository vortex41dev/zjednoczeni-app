import React, {useEffect} from 'react';
import './App.scss';

import Paperbase from './Paper'
import {register} from "./serviceWorker";

function App() {

  useEffect(()=>{
    register();
  },[]);

  return (
    <div className="App">
      <Paperbase />
    </div>
  );
}

export default App;
