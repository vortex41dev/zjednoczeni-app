import React, {ReactNode} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {AuthContextProvider} from "../Users/AuthenticationContext";

import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider} from "@material-ui/pickers";


const styles = (theme: Theme) =>
  createStyles({});

interface ContextStoreProps extends WithStyles<typeof styles> {
  children?: ReactNode;
}

function ContextStore(props: ContextStoreProps) {
  const {classes} = props;

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <AuthContextProvider>
        {props.children}
      </AuthContextProvider>
    </MuiPickersUtilsProvider>
  );
}

export default withStyles(styles)(ContextStore);
