import React, {ReactElement, ReactNode} from 'react';
import {createStyles, Paper, Theme, Typography, withStyles, WithStyles} from "@material-ui/core";


const styles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    root: {
      width: '100%',
      overflowX: 'auto',
      maxWidth: 936,
      alignSelf: 'center',
    },
    root_fw: {
      width: '100%',
      overflowX: 'auto',
      alignSelf: 'center',
    },
    headerTitle: {
      fontSize: 24,
      marginBottom: 15,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });


export interface CardWithTitleProps extends WithStyles<typeof styles> {
  children?: ReactNode;
  title: ReactElement|string;
  fullWidth?: boolean;
}

function CardWithTitle(props: CardWithTitleProps) {
  const {classes} = props;
  return (
    <div className={classes.container}>
      <Typography variant="h1" color="textSecondary" align="center" className={classes.headerTitle}>
        {props.title}
      </Typography>
      <Paper className={props.fullWidth ? classes.root_fw : classes.root}>
        {props.children}
      </Paper>
    </div>
  );
}

export default withStyles(styles)(CardWithTitle);
