import React from 'react';
import {CircularProgress, createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import CardWithTitle from "../Layout/CardWithTitle";


const styles = (theme: Theme) =>
  createStyles({
    progress: {
      margin: theme.spacing(2),
    },
  });

interface LoadingIndicatorProps extends WithStyles<typeof styles> {
  title?: string;
}

function LoadingIndicatorWithCard(props: LoadingIndicatorProps) {
  const title = props.title || "Ładowanie...";
  const {classes} = props;

  return (
    <CardWithTitle title={title}>

      <CircularProgress className={classes.progress} />

    </CardWithTitle>
  );
}

export default withStyles(styles)(LoadingIndicatorWithCard);
