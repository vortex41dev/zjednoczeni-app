import React from 'react';
import {CircularProgress, createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import CardWithTitle from "../Layout/CardWithTitle";
import Typography from "@material-ui/core/Typography";


const styles = (theme: Theme) =>
  createStyles({
    progress: {
      margin: theme.spacing(2),
    },
    title: {
      fontSize: 24,
      marginTop: 15,
      marginBottom: 15,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  });

interface LoadingIndicatorProps extends WithStyles<typeof styles> {
  title?: string;
}

function LoadingIndicator(props: LoadingIndicatorProps) {
  const title = props.title || "Ładowanie...";
  const {classes} = props;

  return (
    <div className={classes.container}>
      <Typography variant="h4" color="textSecondary" align="center" className={classes.title}>
        {title}
      </Typography>
      <CircularProgress className={classes.progress} />
    </div>
  );
}

export default withStyles(styles)(LoadingIndicator);
