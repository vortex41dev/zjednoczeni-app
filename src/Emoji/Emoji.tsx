import React from 'react';
import {createStyles, Theme, WithStyles} from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = (theme: Theme) =>
  createStyles({
    emoji: {
      display: 'inline-block',
      width: 20,
      textAlign: 'center',
    },
  });

export interface EmojiProps extends WithStyles<typeof styles> {
  label: string;
  symbol: string;
  className?: string;
}

function Emoji(props: EmojiProps) {
  const {classes} = props;
  return (
    <span
      className={classes.emoji + ' ' + (props.className || 'emoji')}
      role="img"
      aria-label={props.label ? props.label : ""}
      aria-hidden={props.label ? "false" : "true"}
    >
        {props.symbol}
    </span>
  );
}

export default withStyles(styles)(Emoji);
