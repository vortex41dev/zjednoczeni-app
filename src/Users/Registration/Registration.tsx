import React from 'react';

import {createStyles, Paper, Theme, Typography, withStyles, WithStyles} from "@material-ui/core";
import {ApiClient, UserData} from "../../ZjednoczeniApi/Client";
import RegistrationForm from "./Form";

import CheckIcon from '@material-ui/icons/CheckCircle';

const styles = (theme: Theme) =>
  createStyles({
    paper: {
      maxWidth: 936,
      margin: 'auto',
      overflow: 'hidden',
    },
    searchBar: {
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    },
    searchInput: {
      fontSize: theme.typography.fontSize,
    },
    block: {
      display: 'block',
    },
    addUser: {
      marginRight: theme.spacing(1),
    },
    contentWrapper: {
      margin: '40px 16px',
    },
    headerTitle: {
      fontSize: 24,
      marginBottom: 15,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    errorMessage: {
      fontSize: 14,
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
    },
    messageIcon: {
      marginRight: theme.spacing(2),
      color: '#00c853'
    }
  });

export interface RegistrationProps extends WithStyles<typeof styles> {
  apiClient: ApiClient;
}

interface MessageBoxProps extends WithStyles<typeof styles> {
  message: string;
}

function _MessageBox(props: MessageBoxProps) {
  const {classes} = props;

  return (
    <React.Fragment>
      <Typography variant="h4" color="textPrimary" align="center"
                  className={classes.headerTitle}>
        <CheckIcon className={classes.messageIcon} />
        Rejestracja przebiegła pomyślnie!
      </Typography>
      <Typography variant="h4" color="textPrimary" align="center"
                  className={classes.headerTitle}>{props.message}</Typography>
    </React.Fragment>
  );
}

const MessageBox = withStyles(styles)(_MessageBox);

function RegistrationContent(props: RegistrationProps) {
  const {classes} = props;

  const [values, setValues] = React.useState({
    registered: false,
    registeredMessage: '',
  });

  const onRegistered = (user: UserData) => {
    setValues({...values, registered: true, registeredMessage: `Witamy na pokładzie, ${user.username}! :)`})
  };

  return (
    <React.Fragment>
      <Typography variant="h1" color="textSecondary" align="center"
                  className={classes.headerTitle}>Rejestracja</Typography>
      <Paper className={classes.paper}>
        <div className={classes.contentWrapper}>
          {values.registered
            ? <MessageBox message={values.registeredMessage} />
            : <RegistrationForm apiClient={props.apiClient} onRegistered={onRegistered} />
          }
        </div>
      </Paper>
    </React.Fragment>
  );
}

export default withStyles(styles)(RegistrationContent);
