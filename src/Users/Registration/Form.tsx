import React from 'react';

import {
  Button,
  CircularProgress,
  createStyles,
  TextField,
  Theme,
  Typography,
  withStyles,
  WithStyles
} from "@material-ui/core";
import {ApiClient, UserData} from "../../ZjednoczeniApi/Client";
import {validateEmail, validatePassword, validateUsername} from "../Utils/Validation";


const styles = (theme: Theme) =>
  createStyles({
    form: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    errorMessage: {
      fontSize: 14,
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
    },
    buttonProgress: {
      marginLeft: 15,
    }
  });


export interface RegistrationFormProps extends WithStyles<typeof styles> {
  apiClient: ApiClient;
  onRegistered: (u: UserData) => any;
}


function RegistrationForm(props: RegistrationFormProps) {

  const {classes} = props;

  const [values, setValues] = React.useState({
    username: 'test',
    usernameError: '',
    email: 'test@test.pl',
    emailError: '',
    password: 'asdfasdf',
    passwordRep: 'asdfasdf',
    passwordError: '',
    loading: false,
  });

  const handleChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({...values, [name]: event.target.value});
  };

  const validateForm = (values: any) => {
    const errors: any = {};
    if (!validateUsername(values.username)) {
      errors.usernameError = "Wpisz nazwę użytkownika";
    }
    if (!validateEmail(values.email)) {
      errors.emailError = "Nieprawidłowy email";
    }
    if (!validatePassword(values.password)) {
      errors.passwordError = "Nieprawidłowe hasło, wpisz co najmniej 8 znaków.";
    } else if (values.password !== values.passwordRep) {
      errors.passwordError = "Wpisane hasła nie są identyczne";
    }
    return Object.keys(errors).length > 0 ? errors : false;
  };

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    const result = validateForm(values);
    if (!result) {
      setValues({...values, usernameError: '', emailError: '', passwordError: '', loading: true});
      props.apiClient.register({
        username: values.username,
        email: values.email,
        plainPassword: values.password,
      }).then((u: UserData) => {
        setValues({...values, usernameError: '', emailError: '', passwordError: '', loading: false});
        props.onRegistered(u);
      }).catch(e => {
        console.log(e);
        let usernameErr = e.errors.children.username && e.errors.children.username.errors ? e.errors.children.username.errors[0] : null;
        let emailErr = e.errors.children.email && e.errors.children.email.errors ? e.errors.children.email.errors[0] : null;
        let passwordErr = e.errors.children.plainPassword && e.errors.children.plainPassword.errors ? e.errors.children.plainPassword.errors[0] : null;

        setValues({...values, usernameError: usernameErr, emailError: emailErr, passwordError: passwordErr});
      });
    } else {
      setValues({...values, usernameError: '', emailError: '', passwordError: '', ...result});
    }
  };


  return (
    <form className={classes.form} noValidate autoComplete="off" onSubmit={handleSubmit}>
      <TextField
        id="username-input"
        label="Wyświetlana nazwa użytkownika"
        className={classes.textField}
        value={values.username}
        onChange={handleChange('username')}
        error={!!values.usernameError}
        margin="normal"
        fullWidth
        variant="outlined"
        InputProps={{
          readOnly: values.loading,
        }}
      />
      {(values.usernameError ? <Typography variant="h3" className={classes.errorMessage}
                                           color="error">{values.usernameError}</Typography> : null)}
      <TextField
        id="email-input"
        label="Email"
        value={values.email}
        error={!!values.emailError}
        className={classes.textField}
        onChange={handleChange('email')}
        fullWidth
        type="email"
        margin="normal"
        variant="outlined"
        InputProps={{
          readOnly: values.loading,
        }}
      />
      {(values.emailError ? <Typography variant="h3" className={classes.errorMessage}
                                        color="error">{values.emailError}</Typography> : null)}


      <TextField
        id="password-input"
        label="Hasło"
        value={values.password}
        error={!!values.passwordError}
        className={classes.textField}
        onChange={handleChange('password')}
        fullWidth
        type="password"
        margin="normal"
        variant="outlined"
        InputProps={{
          readOnly: values.loading,
        }}
      />
      <TextField
        id="password-rep-input"
        label="Powtórz hasło"
        value={values.passwordRep}
        error={!!values.passwordError}
        className={classes.textField}
        onChange={handleChange('passwordRep')}
        fullWidth
        type="password"
        margin="normal"
        variant="outlined"
        InputProps={{
          readOnly: values.loading,
        }}
      />
      {(values.passwordError ? <Typography variant="h3" className={classes.errorMessage}
                                           color="error">{values.passwordError}</Typography> : null)}

      <Button type="submit" variant="contained" color="primary" className={classes.button} fullWidth size="large" disabled={values.loading}>
        Zarejestruj
        {values.loading ? <CircularProgress size={20} className={classes.buttonProgress} color="primary" /> : null}
      </Button>
    </form>
  );
}

export default withStyles(styles)(RegistrationForm);
