import React, {useEffect, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {ApiClient, UserData} from "../../ZjednoczeniApi/Client";
import CardWithTitle from "../../Layout/CardWithTitle";
import UsersList from "./UsersList";
import LoadingIndicator from "../../Layout/LoadingIndicator";


const styles = (theme: Theme) =>
  createStyles({});

interface UsersListPageProps extends WithStyles<typeof styles> {
  apiClient: ApiClient,
}

function UsersListPage(props: UsersListPageProps) {
  const {classes} = props;

  const [users, setUsers] = useState<UserData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    props.apiClient.getUsers()
      .then(users => {
        setUsers(users);
        setLoading(false);
      });
  }, [props.apiClient]);

  return (
    <CardWithTitle title="Użytkownicy">
      {loading ? <LoadingIndicator /> : <UsersList users={users} />
      }
    </CardWithTitle>
  );
}

export default withStyles(styles)(UsersListPage);
