import React, {useState} from 'react';

import {
  AppBar,
  Button,
  createStyles,
  Grid,
  IconButton,
  TextField,
  Theme,
  Toolbar,
  Tooltip,
  Typography,
  withStyles,
  WithStyles
} from "@material-ui/core";

import RefreshIcon from '@material-ui/icons/Refresh';
import SearchIcon from '@material-ui/icons/Search';
import {UserData} from "../../ZjednoczeniApi/Client";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import {Link} from "react-router-dom";


const styles = (theme: Theme) =>
  createStyles({
    paper: {
      maxWidth: 936,
      margin: 'auto',
      overflow: 'hidden',
    },
    searchBar: {
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    },
    searchInput: {
      fontSize: theme.typography.fontSize,
    },
    block: {
      display: 'block',
    },
    addUser: {
      marginRight: theme.spacing(1),
    },
    contentWrapper: {
      margin: '10px 16px 20px',
    },
    table: {
      minWidth: 600,
    },
    link: {
      textDecoration: 'none',
      color: theme.palette.primary.main,
    }
  });

export interface ContentProps extends WithStyles<typeof styles> {
  users: UserData[];
}

function UsersList(props: ContentProps) {
  const {classes, users} = props;

  const [filterPhrase, setFilterPhrase] = useState<string>('');
  const filterPhraseLc = filterPhrase.toLocaleLowerCase();

  return (
    <>
      <AppBar className={classes.searchBar} position="static" color="default" elevation={0}>
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon className={classes.block} color="inherit" />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Szukaj po nazwie lub emailu"
                InputProps={{
                  disableUnderline: true,
                  className: classes.searchInput,
                  value: filterPhraseLc,
                  onChange: (e) => setFilterPhrase(e.target.value),
                }}
              />
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <div className={classes.contentWrapper}>
        {users.length === 0 ?
          <Typography color="textSecondary" align="center">
            Brak użytkowników
          </Typography>
          :
          <Table className={classes.table} aria-label="Modlitwy">
            <TableHead>
              <TableRow>
                <TableCell>Nazwa użytkownika</TableCell>
                <TableCell>Email</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.filter((u: UserData) => u.username.toLocaleLowerCase().indexOf(filterPhraseLc) !== -1 || u.email.toLocaleLowerCase().indexOf(filterPhraseLc) !== -1).map((user: UserData) => {
                return (
                  <TableRow key={user.id}>
                    <TableCell component="th" scope="row">
                      <Link to={`uzytkownicy/${user.id}`} className={classes.link}>
                        {user.username}
                      </Link>
                    </TableCell>
                    <TableCell><a href={`mailto:${user.email}`} className={classes.link}>{user.email}</a></TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        }
      </div>
    </>
  );
}

export default withStyles(styles)(UsersList);
