import React from 'react';

import {createStyles, Paper, Theme, Typography, withStyles, WithStyles} from "@material-ui/core";
import {ApiClient, OAuthTokenInfo} from "../../ZjednoczeniApi/Client";
import LoginForm from './Form';
import {AuthenticationContext, CurrentUserContextData} from "../AuthenticationContext";


const styles = (theme: Theme) =>
  createStyles({
    paper: {
      maxWidth: 936,
      margin: 'auto',
      overflow: 'hidden',
    },
    searchBar: {
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    },
    searchInput: {
      fontSize: theme.typography.fontSize,
    },
    block: {
      display: 'block',
    },
    addUser: {
      marginRight: theme.spacing(1),
    },
    contentWrapper: {
      margin: '40px 16px',
    },
    headerTitle: {
      fontSize: 24,
      marginBottom: 15,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    errorMessage: {
      fontSize: 14,
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
    },
    messageIcon: {
      marginRight: theme.spacing(2),
      color: '#00c853'
    }
  });

export interface LoginFormContentProps extends WithStyles<typeof styles> {
  apiClient: ApiClient;
}

function LoginFormContent(props: LoginFormContentProps) {
  const {classes} = props;

  return (
    <AuthenticationContext.Consumer>{(context: CurrentUserContextData) => {
      const onLoggedIn = (tokenInfo: OAuthTokenInfo) => {
        context.onAuthorized(tokenInfo);
      };

      return (
        <React.Fragment>
          <Typography variant="h1" color="textSecondary" align="center"
                      className={classes.headerTitle}>Zaloguj</Typography>
          <Paper className={classes.paper}>
            <div className={classes.contentWrapper}>
              <LoginForm apiClient={props.apiClient} onLoggedIn={onLoggedIn} />
            </div>
          </Paper>
        </React.Fragment>
      );
    }}
    </AuthenticationContext.Consumer>
  );
}

export default withStyles(styles)(LoginFormContent);
