import React from 'react';

import {
  Button,
  CircularProgress,
  createStyles,
  TextField,
  Theme,
  Typography,
  withStyles,
  WithStyles
} from "@material-ui/core";
import {ApiClient, OAuthTokenInfo} from "../../ZjednoczeniApi/Client";


const styles = (theme: Theme) =>
  createStyles({
    form: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    errorMessage: {
      fontSize: 14,
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
    },
    buttonProgress: {
      marginLeft: 15,
    }
  });


export interface LoginFormProps extends WithStyles<typeof styles> {
  apiClient: ApiClient;
  onLoggedIn: (u: OAuthTokenInfo) => any;
}

function validatePassword(password: string) {
  return password.length >= 8;
}

function validateUsername(username: string) {
  return username.length >= 2;
}


function LoginForm(props: LoginFormProps) {

  const {classes} = props;

  const [values, setValues] = React.useState({
    username: 'test_user',
    usernameError: '',
    password: 'Siemano@123',
    passwordError: '',
    loading: false,
  });

  const handleChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({...values, [name]: event.target.value});
  };

  const validateForm = (values: any) => {
    const errors: any = {};
    if (!validateUsername(values.username)) {
      errors.usernameError = "Wpisz nazwę użytkownika";
    }
    if (!validatePassword(values.password)) {
      errors.passwordError = "Nieprawidłowe hasło, wpisz co najmniej 8 znaków.";
    }
    return Object.keys(errors).length > 0 ? errors : false;
  };

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    const result = validateForm(values);
    if (!result) {
      setValues({...values, usernameError: '', passwordError: '', loading: true});
      props.apiClient.login(values.username, values.password)
        .then((tokenInfo: OAuthTokenInfo) => {
          setValues({...values, usernameError: '', passwordError: '', loading: false});
          props.onLoggedIn(tokenInfo);
        }).catch(e => {
        console.log(e);
        let usernameErr = e.errors.children.username && e.errors.children.username.errors ? e.errors.children.username.errors[0] : null;

        setValues({...values, usernameError: usernameErr});
      });
    } else {
      setValues({...values, usernameError: result.usernameErr, emailError: '', passwordError: '', ...result});
    }
  };


  return (
    <form className={classes.form} noValidate autoComplete="off" onSubmit={handleSubmit}>
      <TextField
        id="username-input"
        label="Nazwa użytkownika"
        className={classes.textField}
        value={values.username}
        onChange={handleChange('username')}
        error={!!values.usernameError}
        margin="normal"
        fullWidth
        variant="outlined"
        InputProps={{
          readOnly: values.loading,
        }}
      />
      {(values.usernameError ? <Typography variant="h3" className={classes.errorMessage}
                                           color="error">{values.usernameError}</Typography> : null)}
      <TextField
        id="password-input"
        label="Hasło"
        value={values.password}
        error={!!values.passwordError}
        className={classes.textField}
        onChange={handleChange('password')}
        fullWidth
        type="password"
        margin="normal"
        variant="outlined"
        InputProps={{
          readOnly: values.loading,
        }}
      />
      {(values.passwordError ? <Typography variant="h3" className={classes.errorMessage}
                                           color="error">{values.passwordError}</Typography> : null)}

      <Button type="submit" variant="contained" color="primary" className={classes.button} fullWidth size="large"
              disabled={values.loading}>
        Zaloguj
        {values.loading ? <CircularProgress size={20} className={classes.buttonProgress} color="primary" /> : null}
      </Button>
    </form>
  );
}

export default withStyles(styles)(LoginForm);
