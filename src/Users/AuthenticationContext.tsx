import React, {Component, createContext} from 'react';
import {ApiClient, OAuthTokenInfo, UserData} from "../ZjednoczeniApi/Client";


export interface CurrentUserContextState {
  user?: UserData | null;
  accessToken?: string | null;
  accessTokenExpDate?: Date | null;
  refreshToken?: string | null;
  api: ApiClient
}

export interface CurrentUserContextData extends CurrentUserContextState {
  onAuthorized: (token: OAuthTokenInfo) => void;
  setUser: (user: UserData) => void;
  reloadUser: () => void;
}

export const AuthenticationContext = createContext<CurrentUserContextData>({
  user: null,
  setUser: (u: UserData) => {
  },
  onAuthorized: (u: OAuthTokenInfo) => {
  },
  reloadUser: () => {
  },
  accessToken: sessionStorage.access_token ? sessionStorage.access_token.access_token : null,
  accessTokenExpDate: sessionStorage.access_token ? new Date(sessionStorage.access_token.expires) : null,
  refreshToken: localStorage.refresh_token,
  api: ApiClient.create(),
});

export class AuthContextProvider extends Component<any, CurrentUserContextState> {
  state: CurrentUserContextState = {
    user: null,
    accessToken: sessionStorage.access_token ? sessionStorage.access_token.access_token : null,
    accessTokenExpDate: sessionStorage.access_token ? new Date(sessionStorage.access_token.expires) : null,
    refreshToken: localStorage.refresh_token,
    api: ApiClient.create(),
  };

  setUser = (user: UserData) => {
    this.setState({user: user});
  };

  loadUser = (authToken: string) => {
    return this.state.api.getCurrentUser(authToken).then((u: UserData) => {
      this.setUser(u);
      return u;
    });
  };

  authUserWithRefreshToken = (refreshToken: string) => {
    console.log('Try login with refresh token');
    this.state.api.loginWithRefreshToken(refreshToken)
      .then((tokenInfo: OAuthTokenInfo) => {
        this.onAuthorized(tokenInfo);
      });
  };

  onAuthorized = (tokenInfo: OAuthTokenInfo) => {
    const accTokenExpDate = new Date();
    accTokenExpDate.setSeconds(accTokenExpDate.getSeconds() + tokenInfo.expires_in);
    sessionStorage.access_token = JSON.stringify({
      access_token: tokenInfo.access_token,
      expires: accTokenExpDate.toString()
    });
    localStorage.refresh_token = tokenInfo.refresh_token;

    this.setState({
      refreshToken: tokenInfo.refresh_token,
      accessToken: tokenInfo.access_token,
      accessTokenExpDate: accTokenExpDate,
      api: ApiClient.create(),
    });
    this.loadUser(tokenInfo.access_token);
  };

  auth() {
    if (this.state.user) {
      return;
    }
    if (this.state.accessToken && this.state.accessTokenExpDate && +(new Date()) < +(this.state.accessTokenExpDate) + 30 * 60) {
      this.loadUser(this.state.accessToken);
      return;
    }
    if (this.state.refreshToken) {
      this.authUserWithRefreshToken(this.state.refreshToken);
      return;
    }
  }

  reloadUser = () => {
    if (this.state.accessToken && this.state.accessTokenExpDate && +(new Date()) < +(this.state.accessTokenExpDate) + 30 * 60) {
      return this.loadUser(this.state.accessToken);
    }
    return null;
  };

  componentDidMount() {
    this.auth();
  }

  componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<CurrentUserContextState>, snapshot?: any): void {
    this.auth();
  }

  render() {
    return (
      <AuthenticationContext.Provider value={{...this.state, setUser: this.setUser, onAuthorized: this.onAuthorized, reloadUser: this.reloadUser}}>
        {this.props.children}
      </AuthenticationContext.Provider>
    );
  }
}
