import React, {useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {ApiClient, UserData} from "../../ZjednoczeniApi/Client";
import useForm from "react-hook-form";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";


const styles = (theme: Theme) =>
  createStyles({
    form: {
      margin: '40px 16px',
      alignSelf: 'center',
    },
    form_row: {
      margin: '10px 8px',
      display: 'flex',
      justifyContent: 'flex-start',
    }
  });

interface UserNotificationSettingsFormProps extends WithStyles<typeof styles> {
  user: UserData;
  apiClient: ApiClient;
  onUserUpdated: (user: UserData) => void;
}

function UserNotificationSettingsForm(props: UserNotificationSettingsFormProps) {
  const {classes, user} = props;

  const {register, handleSubmit, errors} = useForm();

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const onSubmit = (values: any) => {
    const userT: UserData = {...props.user};
    userT.username = values.username;
    userT.email = values.email;
    userT.takes_part_in_prayer = values.takes_part_in_prayer;
    userT.meeting_notifications_enabled = values.meeting_notifications_enabled;

    setIsLoading(true);

    return props.apiClient
      .updateUser(userT)
      .then((user: UserData) => {
        props.onUserUpdated(user);
        return user;
      })
      .then(u => Promise.resolve())
      .catch(reason => {
        // TODO: handle error messages!
        console.log(reason);
      })
      .then(() => {
        setIsLoading(false);
      });
  };

  const errorMsgs: any = {
    username: {
      required: 'To pole jest wymagane',
      minLength: 'Wpisz co najmniej 5 znaków',
    },
    email: {
      required: 'To pole jest wymagane',
      minLength: 'Wpisz co najmniej 5 znaków',
    },
  };

  const errorsFor = (fieldName: string) => {
    const fieldErrors = errors[fieldName];
    if (!fieldErrors) {
      return null;
    }
    if (fieldErrors.message) {
      return fieldErrors.message;
    }
    return errorMsgs[fieldName][fieldErrors.type];
  };

  const validateEmailRemote = async (email: any): Promise<string> => {
    return props.apiClient.validateUsernameEmail(user.id, {email: email})
      .then(() => {
        return true;
      })
      .catch((result: any) => {
        return result.email;
      })
  };
  const validateUsernameRemote = async (username: any): Promise<any> => {
    return props.apiClient.validateUsernameEmail(user.id, {username: username})
      .then(() => {
        return true;
      })
      .catch((result: any) => {
        return result.username;
      })
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
      <Grid container justify="space-around" direction="column">
        <div className={classes.form_row}>
          <TextField variant="outlined" fullWidth
                     name="username"
                     label="Nazwa użytkownika"
                     inputRef={register({required: true, minLength: 5, validate: validateUsernameRemote})}
                     defaultValue={user.username}
                     error={!!errors.username}
                     helperText={errorsFor('username')} />
        </div>
        <div className={classes.form_row}>
          <TextField type="email" variant="outlined" fullWidth
                     name="email"
                     label="Email"
                     inputRef={register({required: true, validate: validateEmailRemote})}
                     defaultValue={user.email}
                     error={!!errors.email}
                     helperText={errorsFor('email')}
          />
        </div>
        <div className={classes.form_row}>
          <FormControlLabel
            control={
              <Checkbox
                name="takes_part_in_prayer"
                defaultChecked={user.takes_part_in_prayer}
                color="primary"
                inputRef={register}
              />
            }
            label="Chcę brać udział w modlitwie we wspólnej intencji wspólnoty"
          />
        </div>
        <div className={classes.form_row}>
          <FormControlLabel
            control={
              <Checkbox
                name="meeting_notifications_enabled"
                defaultChecked={user.meeting_notifications_enabled}
                color="primary"
                inputRef={register}
              />
            }
            label="Powiadomienia o wydarzeniach"
          />
        </div>
        <div className={classes.form_row}>
          <Button type="submit"
                  variant="contained"
                  color="primary"
                  size="large"
                  fullWidth>Zapisz</Button>
        </div>
      </Grid>
    </form>
  );
}

export default withStyles(styles)(UserNotificationSettingsForm);
