import React, {useCallback, useContext, useEffect, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import CardWithTitle from "../../Layout/CardWithTitle";
import {AuthenticationContext} from "../AuthenticationContext";
import UserNotificationSettingsForm from "./UserNotificationSettingsForm";
import {UserData} from "../../ZjednoczeniApi/Client";
import LoadingIndicatorWithCard from "../../Layout/LoadingIndicatorWithCard";
import {RouteComponentProps} from 'react-router-dom';


const styles = (theme: Theme) =>
  createStyles({});

interface UserNotificationSettingsPageProps extends WithStyles<typeof styles>, RouteComponentProps<{ id: string }> {
}

function UserNotificationSettingsPage(props: UserNotificationSettingsPageProps) {
  const {classes} = props;
  const id = parseInt(props.match.params.id);

  const context = useContext(AuthenticationContext);

  const [user, setUser] = useState<UserData | null>(null);

  useEffect(() => {
    context.api.getUser(id)
      .then((u: UserData) => {
        setUser(u);
      })
  }, [context, id]);

  const onUserUpdated = useCallback((user: UserData) => {
    if (context.user && user.id === context.user.id) {
      context.reloadUser();
    }
  }, [context]);

  if (!user) {
    return (
      <LoadingIndicatorWithCard />
    );
  }

  return (
    <CardWithTitle title="Ustawienia powiadomień">
      <UserNotificationSettingsForm user={user} apiClient={context.api} onUserUpdated={onUserUpdated} />
    </CardWithTitle>
  );
}

export default withStyles(styles)(UserNotificationSettingsPage);
