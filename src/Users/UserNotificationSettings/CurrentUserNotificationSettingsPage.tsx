import React, {useCallback, useContext} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import CardWithTitle from "../../Layout/CardWithTitle";
import {AuthenticationContext} from "../AuthenticationContext";
import UserNotificationSettingsForm from "./UserNotificationSettingsForm";
import {UserData} from "../../ZjednoczeniApi/Client";


const styles = (theme: Theme) =>
  createStyles({});

interface UserNotificationSettingsPageProps extends WithStyles<typeof styles> {
}

function CurrentUserNotificationSettingsPage(props: UserNotificationSettingsPageProps) {
  const {classes} = props;

  const context = useContext(AuthenticationContext);

  const onUserUpdated = useCallback((user: UserData) => {
    context.reloadUser();
  }, [context]);

  if (!context.user) {
    return (
      <CardWithTitle title="Błąd">
        <span>Brak danych o użytkowniku</span>
      </CardWithTitle>
    );
  }

  return (
    <CardWithTitle title="Ustawienia powiadomień">
      <UserNotificationSettingsForm user={context.user} apiClient={context.api} onUserUpdated={onUserUpdated} />
    </CardWithTitle>
  );
}

export default withStyles(styles)(CurrentUserNotificationSettingsPage);
