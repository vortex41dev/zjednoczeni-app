import {UserData, UserDataC} from "../ZjednoczeniApi/Client";

export interface Article {
  id: number;
  title: string;
  body: string | null;
  creation_date: string;
  author: UserData;
}

export class ArticleC implements Article {
  author: UserData = new UserDataC();
  body: string | null = '';
  creation_date: string = '';
  id: number = 0;
  title: string = '';
}
