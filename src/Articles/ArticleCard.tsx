import React, {useContext, useState} from 'react';
import {
  Avatar,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  createStyles,
  IconButton,
  Paper,
  Theme,
  Typography,
  withStyles,
  WithStyles
} from "@material-ui/core";
import clsx from 'clsx';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {red} from "@material-ui/core/colors";
import {Article} from "./Article";
import {AuthenticationContext} from "../Users/AuthenticationContext";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {Link} from "react-router-dom";


const styles = (theme: Theme) =>
  createStyles({
    cardHeader: {
      textAlign: 'left',
      marginBottom: 15,
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  });

interface ArticleCardProps extends WithStyles<typeof styles> {
  article: Article;
}

function ArticleCard(props: ArticleCardProps) {
  const {classes} = props;

  const [contentExpanded, setContentExpanded] = useState(false);
  const handleExpandClick = () => {
    setContentExpanded(!contentExpanded);
  };

  const [menuAnchorEl, setMenuAnchorEl] = useState<null | HTMLElement>(null);
  const handleContextMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setMenuAnchorEl(event.currentTarget);
  };
  const handleContextMenuClose = () => {
    setMenuAnchorEl(null);
  };

  const context = useContext(AuthenticationContext);

  const shouldDisplayManagementOptions = (context.user && context.user.roles.indexOf('ROLE_ADMIN') !== -1);

  const actions = [];

  if (shouldDisplayManagementOptions) {
    actions.push(
      <React.Fragment key='mgmt'>
        <IconButton aria-label="settings" onClick={handleContextMenuClick}>
          <MoreVertIcon />
        </IconButton>
        <Menu
          id={`article-${props.article.id}-context-menu`}
          anchorEl={menuAnchorEl}
          keepMounted
          open={Boolean(menuAnchorEl)}
          onClose={handleContextMenuClose}
        >
          <Link to={`/admin/aktualnosci/${props.article.id}/edytuj`}>
            <MenuItem onClick={handleContextMenuClose}>Edytuj</MenuItem>
          </Link>
        </Menu>
      </React.Fragment>
    );
  }

  return (
    <Paper className={classes.cardHeader} key={props.article.id}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            {props.article.author.username.substr(0, 1).toLocaleUpperCase()}
          </Avatar>
        }
        action={actions}
        title={props.article.author.username}
        subheader={(new Date(props.article.creation_date)).toLocaleString()}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary">{props.article.title}</Typography>
      </CardContent>
      <Collapse in={contentExpanded} timeout="auto" collapsedHeight='65px'>
        <CardContent onClick={event => {
          if (contentExpanded) {
            return;
          }
          setContentExpanded(true);
        }}>
          <span dangerouslySetInnerHTML={{__html: props.article.body || ''}} />
        </CardContent>
      </Collapse>

      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: contentExpanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={contentExpanded}
          aria-label="Czytaj więcej"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
    </Paper>
  );
}

export default withStyles(styles)(ArticleCard);
