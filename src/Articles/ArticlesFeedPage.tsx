import React, {useContext, useEffect, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {ApiClient} from "../ZjednoczeniApi/Client";
import {Article} from "./Article";
import CardWithTitle from '../Layout/CardWithTitle';
import LoadingIndicator from "../Layout/LoadingIndicator";
import {red} from "@material-ui/core/colors";
import {AuthenticationContext} from "../Users/AuthenticationContext";
import ArticleCard from "./ArticleCard";
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';

const styles = (theme: Theme) =>
  createStyles({
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
    addBtn: {
      position: 'fixed',
      zIndex: 1000,
      right: 15,
      bottom: 15,
    },

    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    root: {
      width: '100%',
      overflowX: 'auto',
      maxWidth: 936,
      alignSelf: 'center',
    },
    root_fw: {
      width: '100%',
      overflowX: 'auto',
      alignSelf: 'center',
    },
    headerTitle: {
      fontSize: 24,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    headerTitleWrapper: {
      marginBottom: 15,
    },
  });

interface ArticlesFeedPageProps extends WithStyles<typeof styles> {
  apiClient: ApiClient,
}

function ArticlesFeedPage(props: ArticlesFeedPageProps) {
  const {classes} = props;

  const [articles, setArticles] = useState<Article[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const context = useContext(AuthenticationContext);

  const reloadArticles = () => {
    setLoading(true);
    context.api.getArticlesList()
      .then((articlesResult: Article[]) => {
        setArticles(articlesResult);
        setLoading(false);
      });
  };
  useEffect(reloadArticles, [props.apiClient]);

  if (loading) {
    return (
      <CardWithTitle title="Aktualności">
        <LoadingIndicator />
      </CardWithTitle>
    );
  }

  return (
    <div className={classes.container}>
      <Grid container alignItems="stretch" className={classes.headerTitleWrapper}>
        <Grid item xs container justify="center" style={{display: 'flex'}}>
          <Typography variant="h1" color="textSecondary" align="center" className={classes.headerTitle}>
            Aktualności
          </Typography>
        </Grid>
      </Grid>
      {context.user && context.user.roles.indexOf('ROLE_ADMIN') !== -1 ?
        <Link to="/admin/aktualnosci/nowa" className={classes.addBtn}>
          <Fab color="primary" aria-label="Dodaj wpis">
            <AddIcon />
          </Fab>
        </Link>
        : null}
      <div className={classes.root}>
        {articles.map(a => <ArticleCard article={a} key={a.id} />)}
      </div>
    </div>
  );

}

export default withStyles(styles)(ArticlesFeedPage);
