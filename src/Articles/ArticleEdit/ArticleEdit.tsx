import React, {useContext, useEffect, useState} from 'react';
import {createStyles, Grid, TextField, Theme, withStyles, WithStyles} from "@material-ui/core";

import useForm from "react-hook-form";
import {ContentState, EditorState} from 'draft-js';
import {formatDateAtom} from "../../utils/dateUtils";
import {Article} from "../Article";
import {AuthenticationContext} from "../../Users/AuthenticationContext";
import Button from "@material-ui/core/Button";
import {KeyboardDatePicker, KeyboardTimePicker} from "@material-ui/pickers";
import {Editor} from "react-draft-wysiwyg";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './articleEditor.scss';


const styles = (theme: Theme) =>
  createStyles({
    form: {
      margin: '40px 16px',
      alignSelf: 'center',
    },
    form_row: {
      margin: '10px 8px',
      display: 'flex',
      justifyContent: 'flex-start',
    },
    editorWrapper: {
      padding: '18.5px 14px',
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: '8px'
    },
    editor: {
      padding: '5px',
    }
  });

interface ArticleEditProps extends WithStyles<typeof styles> {
  article: Article;
}

function ArticleEdit(props: ArticleEditProps) {

  const {classes} = props;
  // @ts-ignore

  const contentBlock = htmlToDraft(props.article.body || '');

  // not ideal for performance, but good enough for now.
  const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
  const defaultEditorState = EditorState.createWithContent(contentState);
  const [editorState, setEditorState] = useState(defaultEditorState);

  const {register, setValue, triggerValidation, getValues, handleSubmit, errors} = useForm();

  const context = useContext(AuthenticationContext);

  const values = getValues();

  const onSubmit = (values: any) => {
    const creationDate = values.creation_date || new Date();
    if (values.creation_date_time) {
      creationDate.setHours(values.creation_date_time.getHours());
      creationDate.setMinutes(values.creation_date_time.getMinutes());
      creationDate.setSeconds(values.creation_date_time.getSeconds());
    }
    const article: Article = {...props.article};
    article.title = values.title;
    article.creation_date = formatDateAtom(creationDate);
    article.body = values.body;

    return context.api
      .updateArticle(article)
      .then(a => Promise.resolve());
  };

  const errorMsgs: any = {
    title: {
      required: 'Wpisz tytuł',
    },
    body: {
      required: 'Wpisz treść',
    },
    creation_date: {
      required: 'Wybierz datę',
    },
  };

  const errorsFor = (fieldName: string) => {
    const fieldErrors = errors[fieldName];
    if (!fieldErrors) {
      return null;
    }
    if (fieldErrors.message) {
      return fieldErrors.message;
    }
    return errorMsgs[fieldName][fieldErrors.type];
  };

  useEffect(() => {
    register({name: 'title'}, {required: true});
    register({name: 'body'}, {required: true});
    register({name: 'creation_date'}, {required: true});
    register({name: 'creation_date_time'}, {required: true});
  }, [register]);

  useEffect(() => {
    if (props.article.creation_date) {
      setValue('creation_date', new Date(props.article.creation_date));
      setValue('creation_date_time', new Date(props.article.creation_date));
    }
    setValue('body', props.article.body || '');
  }, [props.article.creation_date, props.article.body, setValue]);

  return (
    <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
      <Grid container justify="space-around" direction="column">
        <div className={classes.form_row}>
          <TextField variant="outlined"
                     name="title"
                     fullWidth
                     inputRef={register({required: true})}
                     defaultValue={props.article.title}
                     error={!!errors.title}
                     helperText={errorsFor('title')}
                     label="Tytuł"
          />
        </div>
        <div className={classes.form_row}>
          <KeyboardDatePicker
            margin="normal"
            format="yyyy-MM-dd"
            KeyboardButtonProps={{
              'aria-label': 'Ustaw datę utworzenia',
            }}
            name="creation_date"
            label="Do daty"
            inputVariant="outlined"
            error={!!errors.creation_date}
            helperText={errorsFor('creation_date')}
            value={values.creation_date || props.article.creation_date}
            onChange={(val: Date | null) => {
              setValue('creation_date', val);
              triggerValidation({name: 'creation_date'})
            }}
          />
          <KeyboardTimePicker
            format="HH:mm:ss"
            name="creation_date_time"
            margin="normal"
            KeyboardButtonProps={{
              'aria-label': 'Ustaw czas publikacji',
            }}
            label="Czas utworzenia"
            inputVariant="outlined"
            error={!!errors.creation_date_time}
            helperText={errorsFor('creation_date_time')}
            value={values.creation_date_time || (props.article.creation_date ? new Date(props.article.creation_date) : null)}
            onChange={(val: Date | null) => {
              setValue('creation_date_time', val);
              triggerValidation({name: 'creation_date_time'})
            }}
          />
        </div>
        <div className={classes.form_row}>
          <Editor
            placeholder="Treść"
            editorState={editorState}
            wrapperClassName={classes.editorWrapper}
            editorClassName={classes.editor}
            defaultEditorState={defaultEditorState}
            onEditorStateChange={setEditorState}
            onContentStateChange={(contentState) => {
              setValue('body', draftToHtml(contentState));
            }}
          />
        </div>
        <div className={classes.form_row}>
          <Button type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  size="large">Zapisz</Button>
        </div>
      </Grid>
    </form>
  );
}

export default withStyles(styles)(ArticleEdit);
