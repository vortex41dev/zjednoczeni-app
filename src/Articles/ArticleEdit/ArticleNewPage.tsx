import React, {useContext, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {Article, ArticleC} from "../Article";
import ArticleEditPageContent from "./ArticleEditPageContent";
import {AuthenticationContext} from "../../Users/AuthenticationContext";


const styles = (theme: Theme) =>
  createStyles({});

interface ArticleNewPageProps extends WithStyles<typeof styles> {
}

function ArticleNewPage(props: ArticleNewPageProps) {
  const {classes} = props;
  const context = useContext(AuthenticationContext);
  const articleC = new ArticleC();
  if (context.user) {
    articleC.author = context.user;
  }
  const [article, setArticle] = useState<Article>(articleC);

  return (
    <ArticleEditPageContent
      title="Nowy wpis"
      article={article}
      apiClient={context.api}
    />
  );
}

export default withStyles(styles)(ArticleNewPage);
