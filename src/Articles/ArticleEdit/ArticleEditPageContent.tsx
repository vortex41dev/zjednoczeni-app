import React from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ArticleDeleteButton from "../ArticleDeleteButton";
import Paper from "@material-ui/core/Paper";
import ArticleEdit from "./ArticleEdit";
import {Article} from "../Article";
import {ApiClient} from "../../ZjednoczeniApi/Client";


const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: 936,
      alignSelf: 'center',
    },
    root_fw: {
      width: '100%',
      overflowX: 'auto',
      alignSelf: 'center',
    },
    headerTitle: {
      fontSize: 24,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    headerRow: {
      marginBottom: 15,
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  });

interface ArticleEditPageContentProps extends WithStyles<typeof styles> {
  article: Article;
  title: string;
  apiClient: ApiClient;
  onDeleted?: () => void;
}

function ArticleEditPageContent(props: ArticleEditPageContentProps) {
  const {classes} = props;

  return (
    <div className={classes.container}>
      <Grid container alignItems="center" className={classes.headerRow}>
        <Grid item xs>
          <Typography variant="h1" color="textSecondary" align="center" className={classes.headerTitle}>
            {props.title}
          </Typography>
        </Grid>
        {props.article.id && props.onDeleted ? (
          <Grid item>
            <ArticleDeleteButton apiClient={props.apiClient} article={props.article} onDeleted={props.onDeleted} />
          </Grid>) : null}
      </Grid>
      <Paper className={classes.root}>
        <ArticleEdit article={props.article} />
      </Paper>
    </div>
  );
}

export default withStyles(styles)(ArticleEditPageContent);
