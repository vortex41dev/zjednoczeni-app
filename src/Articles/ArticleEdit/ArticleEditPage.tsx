import React, {useCallback, useContext, useEffect, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {AuthenticationContext} from "../../Users/AuthenticationContext";
import {Article} from "../Article";
import LoadingIndicator from "../../Layout/LoadingIndicator";
import {RouteComponentProps} from 'react-router-dom';
import ArticleEditPageContent from "./ArticleEditPageContent";


const styles = (theme: Theme) =>
  createStyles({});

interface ArticleEditPageProps extends WithStyles<typeof styles>, RouteComponentProps<{ id: string }> {
}

function ArticleEditPage(props: ArticleEditPageProps) {
  const {classes} = props;
  const context = useContext(AuthenticationContext);
  const id = parseInt(props.match.params.id);

  const [loading, setLoading] = useState<boolean>(true);
  const [article, setArticle] = useState<Article | null>(null);

  useEffect(() => {
    setLoading(true);
    context.api.getArticle(id)
      .then((p: Article) => {
        setArticle(p);
        setLoading(false);
      });
  }, [context.api, id]);

  const onDeleted = useCallback(() => {
    props.history.push('/admin/modlitwy');
  }, [props.history]);

  if (!article || loading) {
    return <LoadingIndicator />;
  }

  return (
    <ArticleEditPageContent
      article={article}
      title={article.title}
      onDeleted={onDeleted}
      apiClient={context.api}
    />
  );

}

export default withStyles(styles)(ArticleEditPage);
