import React, {useCallback, useState} from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {Article} from "./Article";
import {ApiClient} from "../ZjednoczeniApi/Client";
import Button from "@material-ui/core/Button";
import ConfirmDialog from "../Dialog/ConfirmDialog";


const styles = (theme: Theme) =>
  createStyles({});

interface ArticleDeleteButtonProps extends WithStyles<typeof styles> {
  article: Article;
  apiClient: ApiClient;
  onDeleted?: () => void;
}

function ArticleDeleteButton(props: ArticleDeleteButtonProps) {
  const {article, apiClient, onDeleted} = props;

  const [isOpened, setOpened] = useState<boolean>(false);

  const doDeleteArticle = useCallback(() => {
    apiClient.deleteArticle(article.id)
      .then(() => {
        onDeleted && onDeleted();
      });

  }, [onDeleted, article, apiClient]);

  return (
    <>
      <Button type="button" variant="contained" onClick={() => setOpened(true)} color="secondary">Usuń</Button>
      <ConfirmDialog
        name={`article-delete-confirm-${article.id}`}
        isOpened={isOpened}
        title={`Usuwanie wpisu`}
        text={`Na pewno chcesz usunąć wpis "${article.title}?`}
        handleClose={() => setOpened(false)}
        onConfirmed={() => doDeleteArticle()}
      />
    </>
  );
}

export default withStyles(styles)(ArticleDeleteButton);
