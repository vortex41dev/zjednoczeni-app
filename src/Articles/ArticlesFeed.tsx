import React from 'react';
import {createStyles, Theme, withStyles, WithStyles} from "@material-ui/core";
import {ApiClient} from "../ZjednoczeniApi/Client";

const styles = (theme: Theme) =>
  createStyles({});

interface ArticlesFeedProps extends WithStyles<typeof styles> {
  apiClient: ApiClient,
}

function ArticlesFeed(props: ArticlesFeedProps) {
  const {classes} = props;

  return (
    <span>Hello</span>
  );
}

export default withStyles(styles)(ArticlesFeed);
